package com.bookapp.main;

import java.util.List;

import com.bookapp.exceptions.BookNotFoundException;
import com.bookapp.exceptions.IdNotFoundException;
import com.bookapp.model.Book;
import com.bookapp.repository.DbManager;
import com.bookapp.service.BookServiceImpl;
import com.bookapp.service.IBookService;

public class Client {

	public static void main(String[] args) throws BookNotFoundException, IdNotFoundException {
		DbManager.openConection();
		IBookService bookService = new BookServiceImpl();
		
//		System.out.println("* * * For AAding New Book * * ");
//		Book book = new Book("concentration", 6, "vineet", 478, "mindcontrol");
//		bookService.addBook(book);

		//--------------------------------------------------------------------
		try {
			System.out.println("* * * All Books * * *");
			List<Book> allBooks =  bookService.getAll();
			allBooks.forEach(System.out::println);
			
			System.out.println("* * * Book By Author * * *");
			List<Book> bookByAuthor =  bookService.getByAuthor("Vijay");
			bookByAuthor.forEach(System.out::println);
			
			System.out.println("* * * Book By Category * * *");
			List<Book> bookByCategory =  bookService.getByCategory("Games");
			bookByCategory.forEach(System.out::println);
			
			System.out.println("* * * Boo By BookId * * *");
			Book bookById =  bookService.getById(2);
			System.out.println(bookById);
			
			System.out.println("* * * Book By Leasser Price * * *");
			bookService.getByLesserPrice(20).forEach(System.out::println);
			
			System.out.println("* * * Updating the Book * * *");
			bookService.updateBook(1, 100);
			
			System.out.println("* * * Deleting The Book By Id * * *");
			bookService.deleteBook(12);

		} catch (BookNotFoundException e) {
			System.out.println(e.getMessage());
		} catch ( IdNotFoundException e) {
			System.out.println(e.getMessage());
		}
		//---------------------------------------------------------------------
	}
}













