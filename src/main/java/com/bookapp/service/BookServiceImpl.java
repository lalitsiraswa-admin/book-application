package com.bookapp.service;

import java.util.Collections;
import java.util.List;

import com.bookapp.exceptions.BookNotFoundException;
import com.bookapp.exceptions.IdNotFoundException;
import com.bookapp.model.Book;
import com.bookapp.repository.BookRepositoryImpl;
import com.bookapp.repository.IBookRepository;

public class BookServiceImpl implements IBookService {

	IBookRepository bookRepository = new BookRepositoryImpl();

	@Override
	public List<Book> getAll() {
		return bookRepository.findAll();
	}

	@Override
	public List<Book> getByAuthor(String author) throws BookNotFoundException{
		List<Book> bookByAuthor = bookRepository.findByAuthor(author);
		if (bookByAuthor != null)
			Collections.sort(bookByAuthor, (b1, b2) -> b1.getTitle().compareTo(b2.getTitle()));
		return bookByAuthor;
	}

	@Override
	public List<Book> getByCategory(String category) throws BookNotFoundException {
		List<Book> bookByCategory = bookRepository.findByCategory(category);
		if (bookByCategory != null)
			Collections.sort(bookByCategory, (b1, b2) -> b1.getCategory().compareTo(b2.getCategory()));
		return bookByCategory;
	}

	@Override
	public Book getById(int bookId) throws IdNotFoundException {
		return bookRepository.findById(bookId);
	}

	@Override
	public List<Book> getByLesserPrice(double price) throws BookNotFoundException {
		List<Book> bookByLesserPrice = bookRepository.findByLesserPrice(price);
		if (bookByLesserPrice != null)
			Collections.sort(bookByLesserPrice, (b1, b2) -> b1.getTitle().compareTo(b2.getTitle()));
		return bookByLesserPrice;
	}

	@Override
	public void addBook(Book book) {
		bookRepository.addBook(book);	
	}

	@Override
	public void updateBook(Integer bookid, double price) throws IdNotFoundException{
		bookRepository.updateBook(bookid, price);	
	}

	@Override
	public void deleteBook(Integer bookid) throws IdNotFoundException{
		bookRepository.deleteBook(bookid);	
	}
}
