package com.bookapp.repository;

import java.util.List;

import com.bookapp.exceptions.BookNotFoundException;
import com.bookapp.exceptions.IdNotFoundException;
import com.bookapp.model.Book;

public interface IBookRepository {
	void addBook(Book book);

	void updateBook(Integer bookid, double price) throws IdNotFoundException;

	void deleteBook(Integer bookid) throws IdNotFoundException;

	List<Book> findAll();

	List<Book> findByAuthor(String author) throws BookNotFoundException;

	List<Book> findByCategory(String category) throws BookNotFoundException;

	Book findById(int bookId) throws IdNotFoundException;

	List<Book> findByLesserPrice(double price) throws BookNotFoundException;
}
