package com.bookapp.repository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bson.Document;

import com.bookapp.exceptions.BookNotFoundException;
import com.bookapp.exceptions.IdNotFoundException;
import com.bookapp.model.Book;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Updates;

public class BookRepositoryImpl implements IBookRepository {

	DbManager dbManager = new DbManager();
	MongoCollection<Document> collection = dbManager.getCollection();

	@Override
	public List<Book> findAll() {
//	    List<Book> docs = collection.find(new Document(), Book.class).into(new ArrayList<Book>());
		FindIterable<Book> allDocuments = collection.find(new Document(), Book.class);
		List<Book> allBooks = new ArrayList<Book>();
		for (Book docs : allDocuments) {
			System.out.println(docs);
		}
		return allBooks;
	}

	@Override
	public List<Book> findByAuthor(String author) throws BookNotFoundException {
		FindIterable<Book> allDocuments = collection.find(new Document(), Book.class);
		List<Book> bookByAuthor = new ArrayList<Book>();
		for (Book book : allDocuments) {
			if (book.getAuthor().equals(author)) {
				bookByAuthor.add(book);
			}
		}
		if (bookByAuthor.isEmpty() || bookByAuthor == null)
			throw new BookNotFoundException("Book Not Found!");
		return bookByAuthor;
	}

	@Override
	public List<Book> findByCategory(String category) throws BookNotFoundException {
		FindIterable<Book> allDocuments = collection.find(new Document(), Book.class);
		List<Book> bookByCategory = new ArrayList<Book>();
		for (Book book : allDocuments) {
			if (book.getCategory().equals(category)) {
				bookByCategory.add(book);
			}
		}
		if (bookByCategory.isEmpty() || bookByCategory == null)
			throw new BookNotFoundException("Book Not Found!");
		return bookByCategory;
	}

	@Override
	public Book findById(int bookId) throws IdNotFoundException {
		FindIterable<Book> allDocuments = collection.find(new Document(), Book.class);
		Book desiredBook = null;
		for (Book book : allDocuments) {
			if (book.getBookId() == bookId) {
				desiredBook = book;
			}
		}
		if (desiredBook == null)
			throw new IdNotFoundException("Book with id " + bookId + " not found.");
		return desiredBook;
	}

	@Override
	public List<Book> findByLesserPrice(double price) throws BookNotFoundException {
		FindIterable<Book> allDocuments = collection.find(new Document(), Book.class);
		List<Book> booksByLesserPrice = new ArrayList<>();

		for (Book book : allDocuments) {
			if (book.getPrice() < price) {
				booksByLesserPrice.add(book);
			}
		}
		if (booksByLesserPrice.isEmpty())
			throw new BookNotFoundException("Books with price less than " + price + " not found.");
		return booksByLesserPrice;
	}

	@Override
	public void addBook(Book book) {
		Map<String, Object> bookMap = new HashMap<>();
		bookMap.put("title", book.getTitle());
		bookMap.put("_id", book.getBookId());
		bookMap.put("author", book.getAuthor());
		bookMap.put("price", book.getPrice());
		bookMap.put("category", book.getCategory());
		Document bookDoc = new Document(bookMap);
		collection.insertOne(bookDoc);
	}

	@Override
	public void updateBook(Integer bookid, double price) throws IdNotFoundException {

		FindIterable<Book> allDocuments = collection.find(new Document(), Book.class);
		for (Book book : allDocuments) {
			if (book.getBookId() == bookid) {
				collection.updateOne(Filters.eq("_id", bookid), Updates.set("price", price));
				System.out.println("Book with id " + bookid + " has updated to the price " + price);
				return;
			}
		}
		throw new IdNotFoundException("Book with id " + bookid + " not found.");

	}

	@Override
	public void deleteBook(Integer bookid) throws IdNotFoundException {
		FindIterable<Book> allDocuments = collection.find(new Document(), Book.class);
		for (Book book : allDocuments) {
			if (book.getBookId() == bookid) {
				collection.deleteOne(Filters.eq("_id", bookid));
				System.out.println("Book with id " + bookid + " is successfully deleted.");
				return;
			}
		}
		throw new IdNotFoundException("Book with id " + bookid + " not found.");
	}
}
